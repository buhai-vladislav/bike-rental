export const Router = () => ({
    get: async () => {
        const response = await fetch(`/api/get`);
        return response.json();
    },
    add: async bike => {
        const response = await fetch(
            `/api/add`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(bike)
            }
        );

        return response.json();
    },
    update: async bike => {
        const response = await fetch(
            `/api/update`,
            {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(bike)
            }
        );

        return response.json();
    },
    delete: async ({ id }) => {
        const response = await fetch(
            `/api/delete/${id}`,
            {
                method: 'DELETE'
            }
        );

        return response.json();
    }

});