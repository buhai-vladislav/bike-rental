import React, { useEffect, useState } from 'react';

import Form from './components/form/Form';
import RentedList from './components/list/RentedList';
import AvailableList from './components/list/AvailableList';
import Lists from './components/lists/Lists';
import Modal from './components/modal/Modal';

import { Router } from './router';

import './index.css'

function App() {

  const router = Router();
  const [bikesArray, setBikesArray] = useState({
    loading: false,
    data: null
  });
  const [bike, setBike] = useState('');

  const RentedBikesList = Lists(RentedList);
  const AvailableBikesList = Lists(AvailableList);

  useEffect(() => {
    setBikesArray({loading: true});
    router.get().then(bikes => {
      setBikesArray({loading: false, data: bikes});
    })
  }, [setBikesArray]);

  return (
    <div className="wrapper">
      <section>
        <h4>Awesome bike rental</h4>
      </section>
      <section>
        <div className="main-block">
          <Modal data={{id: 'modal', header: 'Text', bikeData: bike}}/>
          <Form data={[bikesArray.data, router, setBikesArray]} />
          <RentedBikesList 
            isLoading={bikesArray.loading} 
            data={[bikesArray.data, router, setBikesArray, setBike]}/>
          <AvailableBikesList 
            isLoading={bikesArray.loading} 
            data={[bikesArray.data, router, setBikesArray]}/>
        </div>
      </section>
    </div>
  );
}

export default App;
