import React, { useState, useEffect } from 'react';
import Modal from '../modal/Modal';
import M from 'materialize-css';

function Form(props) {

  const [bikeName, setBikeName] = useState(null);
  const [bikeType, setBikeType] = useState(null);
  const [rentPrice, setRentPrice] = useState(null);

  useEffect(() => {
    M.AutoInit();
  })

  function handleSubmit(event) {
    event.preventDefault();

    if (bikeName !== null || bikeType !== null || rentPrice !== null) {
      const dataToAdd = {
        id: Date.now().toString(16).slice(-4).padStart(5, 'x'),
        name: bikeName,
        type: bikeType,
        price: rentPrice,
        isRent: false,
        rentDate: 'null'
      }
      const response = props.data[1].add(dataToAdd);
      response.then(() => {
        props.data[0].push(dataToAdd);
        props.data[2]({ data: props.data[0] })
      });
    }
  };

  return (
    <div
      className="row"
      style={{
        marginBottom: 0,
        backgroundColor: '#E9EAEE',
      }}>
      <form className="col s12">
        <div
          className="row valign-wrapper"
          style={{
            marginBottom: 0
          }} >
          <div className="col s3 input-field">
            <label id="bike-name" >Bike name</label>
            <input placeholder="Enter bike name" type="text" onChange={
              event => setBikeName(event.target.value)} />
          </div>
          <div className="col s3 input-field">
            <select onChange={
              event => setBikeType(event.target.value)
            }>
              <option value="custom">Custom</option>
              <option value="road">Road</option>
              <option value="sportbike">Sportbike</option>
              <option value="offRoad">Off-road</option>
            </select>
            <label id="bike-type">Bike type</label>
          </div>
          <div className="col s3 input-field">
            <label id="rent-price">Rent Price</label>
            <input placeholder="Enter rent price" type="number" onChange={
              event => setRentPrice(event.target.value)
            } />
          </div>
          <div className="col s3 right-align">
            <button
              className="btn waves-effect waves-light modal-trigger"
              onClick={event => { handleSubmit(event) }}
              style={{
                minWidth: '125px'
              }}>
              Button
              </button>
          </div>
        </div>
      </form>
    </div>
  );
}

export default Form;