import React from 'react';
const AvailableListElem = (props) => {
    const bike = props.data.notRentedBike;
    return (
        <li key={bike.id} className='list-elem'>
            <div className="row valign-wrapper"
                style={{
                    marginBottom: 0,
                    padding: '10px 0'
                }}>
                <div className="col s5">
                    <span className='repo-text'>{bike.name} / </span>
                    <span className='repo-description'>{bike.type} / </span>
                    <span className='repo-description'>{bike.price}</span>
                </div>
                <div className="col s7 right-align" style={{ paddingRight: 0 }}>
                    <button className="btn btn-blue" 
                        onClick={() => props.data.handleFromAvailable(bike.id)}>
                            Rent
                    </button>
                    <button 
                        className="btn btn-red"
                        data-target="modal1"
                        onClick={() => props.data.handleRemove(bike.id)}
                        >Delete
                    </button>
                </div>
            </div>
        </li>
    );
};
export default AvailableListElem;