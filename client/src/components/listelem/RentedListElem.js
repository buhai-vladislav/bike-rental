import React from 'react';
import Modal from '../modal/Modal';

const RentedListElem = (props) => {
    const bike = props.data.rentedBike;
    return (
        <li key={bike.id} className='list-elem'>
            <div className="row valign-wrapper"
                style={{
                    marginBottom: 0,
                    padding: '10px 0'
                }}>
                <div className="col s6">
                    <span className='repo-text'>{bike.name} / </span>
                    <span className='repo-description'>{bike.type} / </span>
                    <span className='repo-description'>{bike.price}</span>
                </div>
                <div className="col s6 right-align" style={{ paddingRight: 0 }}>
                    <button id={bike.id} className="btn btn-red" 
                        onClick={() => {
                            props.data.handleRemove(bike);
                        }}
                        >Cancel rent
                    </button>
                </div>
            </div>
        </li>
    );
};
export default RentedListElem;