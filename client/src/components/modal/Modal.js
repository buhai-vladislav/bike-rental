import React from 'react';
import { msToTime } from '../../helper/helper'

function Modal(props) {
    const rentedTime = msToTime(new Date().getTime() - new Date(
        Date.parse(props.data.bikeData.rentDate)).getTime());
    const hours = Number.parseInt(rentedTime.slice(0,2));

    const TWENTY_DAYS = 20;

    return (
        <div id={props.data.id} class="modal-rent">
            <div class="modal-content">
                <span
                    class="close"
                    onClick={() => {
                        document.querySelector('#modal').classList.remove('show')
                        console.log('Close');
                    }
                    }
                >&times;</span>
                <p><strong>Name: </strong>{props.data.bikeData.name}</p>
                <p><strong>Type: </strong>{props.data.bikeData.type}</p>
                <p><strong>Price for hour rent: </strong>{props.data.bikeData.price + '$'}</p>
                <p><strong>Rent time: </strong>{ rentedTime }</p>
                <p><strong>Total rent:</strong>{
                    (() => {
                        if(hours <= 0){
                            return props.data.bikeData.price + '$';
                        }
                        if(hours < TWENTY_DAYS) {
                            return Number.parseInt(props.data.bikeData.price) * hours + '$';
                        } else {
                            return `You got a 50% discount for renting more than 20 hours. 
                                Your total rent is: ` + 
                                (Number.parseInt(props.data.bikeData.price) * hours) / 2 + '$';
                        }
                    })()
                    }</p>
            </div>
        </div>
    );
}
export default Modal;