import React, { useState } from 'react';
import AvailableListElem from '../listelem/AvailableListElem'

const AvailableList = (props) => {
    const [availableList, setAvailableList] = useState(props.data[0]);

    function handleFromAvailable(id) {
        const newBikesList = availableList.filter(item => item.id !== id);
        setAvailableList(newBikesList);
        const date = new Date().toString();
        props.data[1].update({
            id: id,
            isRent: true,
            rentDate: date
        });
        const index = props.data[0].findIndex(object => object.id === id);
        props.data[0][index].isRent = true;
        props.data[0][index].rentDate = date;
        props.data[2]({ data: props.data[0] });
    }

    function handleRemove(id) {
        const newBikesList = availableList.filter(item => item.id !== id);
        setAvailableList(newBikesList);
        props.data[1].delete({ id: id });
        props.data[2]({ data: newBikesList });
    }


    if (!availableList || availableList.filter(item => {
        return !item.isRent
    }).length === 0) return <h5>No available bikes</h5>;
    return (
        <ul>
            <h4 className='list-head'>Available Bikes</h4>
            {availableList.filter(bike => { return !bike.isRent })
                .map((notRentedBike) => {
                    return (<AvailableListElem
                        key={notRentedBike.id}
                        data={{ notRentedBike, handleFromAvailable, handleRemove }}
                    />);
                })}
        </ul>
    );
};
export default AvailableList;