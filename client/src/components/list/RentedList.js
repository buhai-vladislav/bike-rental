import React, { useState, useEffect } from 'react';
import RentedListElem from '../listelem/RentedListElem';

const RentedList = (props) => {
  const [bikesList, setBikesList] = useState(props.data[0]);

  function handleRemove(bike) {
    const newBikesList = bikesList.filter(item => item.id !== bike.id);
    setBikesList(newBikesList);
    props.data[1].update({
      id: bike.id,
      isRent: false,
      rentDate: ''
    });
    props.data[0][props.data[0].findIndex(object => object.id === bike.id)].isRent = false;
    props.data[2]({ data: props.data[0] });
    
    props.data[3](bike);
    console.log(bike);
    document.querySelector('#modal').classList.add('show');
  }

  if (!bikesList || bikesList.filter(item => { return item.isRent }).length === 0) return <h5>No rented bikes</h5>;
  return (
    <ul>
      <h4 className='list-head'>Rented Bikes</h4>
      {bikesList.filter(bike => { return bike.isRent }).map((rentedBike) => {
        return (<RentedListElem key={rentedBike.id} data={{ rentedBike, handleRemove }} />);
      })}
    </ul>
  );
};
export default RentedList;