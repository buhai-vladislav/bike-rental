const { Schema, model } = require('mongoose');

const BikeSchema = new Schema({
    id: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        required: true,
    },
    price: {
        type: String,
        required: true
    },
    isRent: {
        type: Boolean,
        required: true
    },
    rentDate: {
        type: String,
        required: true
    }
})

module.exports = model('Bike', BikeSchema);