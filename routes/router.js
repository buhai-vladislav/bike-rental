const router  = require('express').Router();
const Bike = require('../models/Bike');
const { body, validationResult} = require('express-validator');

router.post(
    '/add',
    [
        body('id').exists(),
        body('name').notEmpty().trim().escape(),
        body('type').notEmpty().trim().escape(),
        body('price').notEmpty().trim().escape(),
        body('isRent').toBoolean(),
        body('rentDate').notEmpty().trim().escape()
    ],
    async (req, res) => {
        const errors = validationResult(req);

        if(!errors.isEmpty()){
            return res.status(400).json({ message: errors.array()[0].msg })
        }

        const { id, name, type, price, isRent, rentDate} = req.body;

        const bike = new Bike({
            id,
            name,
            type,
            price,
            isRent,
            rentDate
        });

        try {
            await bike.save();
            return res.status(201).json({ message: 'Bike was added'});
        } catch(error) {
            return res.status(500).json({ message: `Error: ${error}` });
        }
    }
)

router.get('/get', async (req, res) => {
    const bikes = (await Bike.find()) || [];
    return res.json(bikes);
});

router.delete('/delete/:id', async (req, res) => {
    try {
        await Bike.deleteOne({
            id: req.params.id
        });
        return res.status(201).json({ 
            message: `Bike ${req.params.id} was removed` 
        });
    } catch (error) {
        return res.status(500).json({ message: `Error: ${error}` });
    }
})

router.put(
    '/update',
    [
        body('isRent').toBoolean(),
        body('rentDate').trim().escape()
    ],
    async (req, res) => {
        const errors = validationResult(req);

        if(!errors.isEmpty()){
            return res.status(400).json({ message: errors.array()[0].msg });
        }

        const {id, isRent, rentDate} = req.body;

        try {
            await Bike.updateOne(
                {
                    id
                },
                {
                    isRent,
                    rentDate
                }
            );
            return res.status(201).json({ message: `Bike ${id} was updated` });
        } catch(error) {
            return res.status(500).json({message: `Error: ${error}`});
        }
    }
)

module.exports = router;