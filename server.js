const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const path = require('path');

require('dotenv/config');

const app = express();
const router  = require('./routes/router');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

app.use(express.static(path.join(__dirname, './client/build')));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, './client/build', 'index.html'));
})

mongoose.connect(
    process.env.MONGO_URI,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
    },
    () => console.log("Connect to MonogoDB")
);

app.use('/api', router);

const PORT = process.env.PORT || 5000

app.listen(PORT, () => console.log('Server is running'));